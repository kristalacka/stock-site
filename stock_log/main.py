import logging
import argparse
import psycopg2
from psycopg2.extras import execute_values
import pandas as pd
import yfinance as yf
import sched
import time
from datetime import datetime

# news scraping
from bs4 import BeautifulSoup
from urllib.request import urlopen
from urllib.request import Request

# local config
from config import db_config

s = sched.scheduler(time.time, time.sleep)
UPDATE_INTERVAL = 60  # database update interval in seconds
NEWS_INVERVAL = 60 * 60  # news update interval in seconds (1h)


def initialize_database():
    df = pd.read_csv('snp500.csv')
    conn = None
    try:
        conn = psycopg2.connect(host=db_config['host'], database=db_config['database'],
                                user=db_config['user'], password=db_config['password'])

        for row in df.iloc():
            cur = conn.cursor()
            symbol = row['Symbol']
            sector = row['Sector']
            company = row['Name'].replace("'", "''")
            sql = f"INSERT INTO stock_ticker (company_name, ticker_name, last_close_price, views, sector) VALUES ('{company}', '{symbol}', {0.0}, {0}, '{sector}');"
            cur.execute(sql)
            conn.commit()
            cur.close()
    except(Exception, psycopg2.DatabaseError) as err:
        logging.error(err)
    finally:
        if conn is not None:
            conn.close()


def get_ticker_list():
    conn = None
    ticker_list = {}
    try:
        conn = psycopg2.connect(host=db_config['host'], database=db_config['database'],
                                user=db_config['user'], password=db_config['password'])
        cur = conn.cursor()
        print('PostgreSQL database version:')
        sql = 'SELECT id, ticker_name from stock_ticker;'
        cur.execute(sql)
        for row in cur.fetchall():
            ticker_list[row[1]] = row[0]
        cur.close()
    except(Exception, psycopg2.DatabaseError) as err:
        logging.error(err)
    finally:
        if conn is not None:
            conn.close()
    return ticker_list


def update_func(interval):
    s.enter(interval, 1, update_func, (interval,))
    ticker_list = get_ticker_list()

    df_full = yf.download(tickers=list(ticker_list.keys(
    )), period='1d', interval='1m', threads=True, group_by='ticker')
    df_full = df_full.tail(2)  # last ticker updates

    conn = None
    try:
        conn = psycopg2.connect(host=db_config['host'], database=db_config['database'],
                                user=db_config['user'], password=db_config['password'])
        cur = conn.cursor()
        sql = f"INSERT INTO ticker_price (\"timestamp\", price_open, price_close, candle_high, candle_low, volume, fk_stock_ticker_id) VALUES %s \
                    ON CONFLICT ON CONSTRAINT unique_timestamp_ticker DO UPDATE SET price_open=excluded.price_open, price_close = excluded.price_close, \
                        candle_high = excluded.candle_high, candle_low = excluded.candle_low, volume = excluded.volume \
                            WHERE ticker_price.timestamp=excluded.timestamp AND ticker_price.fk_stock_ticker_id=excluded.fk_stock_ticker_id;"
        values_list = []
        for ticker_name, _ in dict(tuple(df_full)).items():
            ticker_id = ticker_list[ticker_name]
            data = df_full[ticker_name]
            for index, row in data.iterrows():
                values = (index, row['Open'], row['Close'],
                          row['High'], row['Low'], row['Volume'], ticker_id)
                values_list.append(values)

        # batch insert
        execute_values(cur, sql, values_list)
        conn.commit()
        cur.close()
    except(Exception, psycopg2.DatabaseError) as err:
        logging.error(err)
    finally:
        if conn is not None:
            conn.close()


def update_data():
    global UPDATE_INTERVAL, NEWS_INVERVAL
    s.enter(UPDATE_INTERVAL, 1, update_func, (UPDATE_INTERVAL,))
    s.enter(NEWS_INVERVAL, 1, insert_news, (NEWS_INVERVAL,))
    s.run()
    # update_func()


def fill_data(ticker):
    conn = None
    ticker_id = None
    try:
        conn = psycopg2.connect(host=db_config['host'], database=db_config['database'],
                                user=db_config['user'], password=db_config['password'])
        cur = conn.cursor()
        print('PostgreSQL database version:')
        sql = f"SELECT id from stock_ticker where ticker_name='{ticker}';"
        print(sql)
        cur.execute(sql)
        ticker_id = cur.fetchone()[0]
        cur.close()
    except(Exception, psycopg2.DatabaseError) as err:
        logging.error(err)
    finally:
        if conn is not None:
            conn.close()

    df_full = yf.download(tickers=ticker, period='1d',
                          interval='1m', threads=True, group_by='ticker')

    conn = None
    try:
        conn = psycopg2.connect(host=db_config['host'], database=db_config['database'],
                                user=db_config['user'], password=db_config['password'])
        cur = conn.cursor()
        sql = f"INSERT INTO ticker_price (\"timestamp\", price_open, price_close, candle_high, candle_low, volume, fk_stock_ticker_id) VALUES %s \
                    ON CONFLICT ON CONSTRAINT unique_timestamp_ticker DO UPDATE SET price_open=excluded.price_open, price_close = excluded.price_close, \
                        candle_high = excluded.candle_high, candle_low = excluded.candle_low, volume = excluded.volume \
                            WHERE ticker_price.timestamp=excluded.timestamp AND ticker_price.fk_stock_ticker_id=excluded.fk_stock_ticker_id;"
        values_list = []
        data = df_full
        for index, row in data.iterrows():
            values = (index, row['Open'], row['Close'],
                      row['High'], row['Low'], row['Volume'], ticker_id)
            values_list.append(values)

        # batch insert
        execute_values(cur, sql, values_list)
        conn.commit()
        cur.close()
    except(Exception, psycopg2.DatabaseError) as err:
        logging.error(err)
    finally:
        if conn is not None:
            conn.close()


def insert_news(interval=None):
    if interval is not None:
        s.enter(interval, 1, insert_news, (interval,))
    finviz_url = 'https://finviz.com/quote.ashx?t='
    ticker_list = get_ticker_list()
    n = 10  # number of headlines for each ticker
    news_tables = {}
    for ticker in ticker_list:
        try:
            url = finviz_url + ticker
            req = Request(url=url, headers={'user-agent': 'my-app/0.0.1'})
            resp = urlopen(req)
            html = BeautifulSoup(resp, features="lxml")
            news_table = html.find(id='news-table')
            news_tables[ticker] = news_table
        except:
            print("could not find ", ticker)
            pass

    sql = f"INSERT INTO stock_news (timestamp, headline, link, fk_stock_ticker_id) VALUES %s ON CONFLICT ON CONSTRAINT unique_link DO NOTHING"
    values_list = []
    for file_name, news_table in news_tables.items():
        i = 0
        for x in news_table.findAll('tr'):
            if i > n:
                break
            i += 1
            text = x.a.get_text()
            link = x.a['href']
            date_scrape = x.td.text.split()

            if len(date_scrape) == 1:
                time_m = date_scrape[0]

            else:
                date = date_scrape[0]
                time_m = date_scrape[1]
            ticker = file_name.split('_')[0]
            datetime_m = date + ' ' + time_m
            timestamp = datetime.strptime(
                datetime_m, "%b-%d-%y %I:%M%p")
            values_list.append(
                (timestamp, text, link, ticker_list[ticker]))

    conn = psycopg2.connect(host=db_config['host'], database=db_config['database'],
                            user=db_config['user'], password=db_config['password'])
    cur = conn.cursor()
    try:
        execute_values(cur, sql, values_list)
        conn.commit()
        cur.close()
    except(Exception, psycopg2.DatabaseError) as err:
        logging.error(err)
    finally:
        if conn is not None:
            conn.close()


logging.basicConfig(level=logging.DEBUG)
parser = argparse.ArgumentParser()
parser.add_argument('--init', dest='init', action='store_true')
parser.add_argument('--fill', dest='fill', action='store')
parser.add_argument('--init_news', dest='init_news', action='store_true')
args = parser.parse_args()

if args.init_news:
    insert_news()
if args.init:
    initialize_database()
if args.fill is not None:
    fill_data(args.fill.upper())
if not args.init_news and not args.init and args.fill is None:
    update_data()
