# from django.shortcuts import render

# Create your views here.

from django.contrib.auth import login
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required, user_passes_test
from .forms import CreateUserForm, EditProfileForm
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.http import JsonResponse
from stock_site.models import *
from django.db.models import F


def staff_required(login_url=None):
    return user_passes_test(lambda u: u.is_staff, login_url=login_url)


def index(request):
    companies = StockTicker.objects.order_by('-views').all()
    context = {'company_list': companies}
    return render(request, 'stock_site/index.html', context)


def dashboard(request, stock_ticker):
    stock = StockTicker.objects.get(ticker_name=stock_ticker)
    stock.views = F("views") + 1
    stock.save(update_fields=["views"])
    message_list = Message.objects.filter(
        fk_stock_ticker_id=stock.id).order_by('-timestamp')
    context = {'company_name': stock.company_name, 'stock_ticker': stock.ticker_name,
               'room_name': stock_ticker, 'messages': list(message_list)}
    return render(request, 'stock_site/dashboard.html', context)


def ticker_list(request):
    return render(request, 'stock_site/ticker_list.html')


def market_resources_list(request):
    resources = MarketResources.objects.all()
    context = {'resource_list': resources}
    return render(request, 'stock_site/resource_list.html', context)


def stock_news_list(request, stock_ticker):
    stock_id = StockTicker.objects.get(ticker_name=stock_ticker).id
    news = StockNews.objects.filter(fk_stock_ticker_id=stock_id)
    context = {'news_list': news, 'stock_ticker': stock_ticker}
    return render(request, 'stock_site/stock_news_list.html', context)


# @user_passes_test(lambda u: u.is_anonymous)
def register(request):
    if request.user.is_authenticated:
        return redirect('index')
    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get('username')
            messages.success(
                request, f"Account for {user} was successfully created. Please log in to continue.")
            return redirect('login')
    else:
        form = CreateUserForm()
    context = {'form': form}
    return render(request, 'registration/register.html', context)


# @user_passes_test(lambda u: u.is_anonymous)
def login_page(request):
    if request.user.is_authenticated:
        return redirect('index')
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('index')
        else:
            messages.info(request, "Username or password is incorrect")
    return render(request, 'registration/login.html', {})


@ login_required(login_url='login')
def profile(request):
    context = {'user': request.user}
    return render(request, 'registration/profile.html', context)


@ login_required(login_url='login')
def edit_profile(request):
    if request.method == 'POST':
        form = EditProfileForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect('profile')
    else:
        form = EditProfileForm(instance=request.user)
        context = {'form': form}
        return render(request, 'registration/edit_profile.html', context)


@ login_required(login_url='login')
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(data=request.POST, user=request.user)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return redirect('profile')
        else:
            return redirect('change_password')
    else:
        form = PasswordChangeForm(user=request.user)
        context = {'form': form}
        return render(request, 'registration/change_password.html', context)


@ login_required(login_url='login')
def logout_page(request):
    logout(request)
    return redirect('login')


def test_page(request):
    return render(request, 'stock_site/test.html')


def user_endpoint(request):
    """Returns `JsonResponse` object"""
    # print(request.GET['username'])
    # TODO get user data by name and add it to hmtl
    if request.is_ajax() and request.method == 'GET':
        resp_data = {
            'html': f"<h4>{request.GET['username']}</h4>",
            # more data
        }

        return JsonResponse(resp_data, status=200)


def stock_data_endpoint(request, stock_ticker):
    stock = StockTicker.objects.get(ticker_name=stock_ticker)
    stock_data = TickerPrice.objects.filter(
        fk_stock_ticker_id=stock.id).values()
    return JsonResponse(list(stock_data), safe=False)


def insert_message(request):
    if request.is_ajax():
        ticker_id = StockTicker.objects.get(
            ticker_name=request.POST.get('stock_ticker')).id
        userid = request.user.id
        Message.objects.create(fk_user_id=request.user.id,
                               fk_stock_ticker_id=ticker_id, text=request.POST.get('msg'))
        return JsonResponse('{}', safe=False)
