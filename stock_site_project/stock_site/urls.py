from django.urls import path
from django.conf.urls import url
from . import views
from django.contrib import admin

urlpatterns = [
    path('', views.index, name='index'),
    path('dashboard/<stock_ticker>/', views.dashboard, name='dashboard'),
    path('ticker_list', views.ticker_list, name='ticker_list'),
    path('market_resources_list', views.market_resources_list,
         name='market_resources_list'),
    path('stock_news_list/<stock_ticker>/',
         views.stock_news_list, name='stock_news_list'),
    path('login', views.login_page, name='login'),
    path('logout', views.logout_page, name='logout'),
    path('register', views.register, name='register'),
    path('profile', views.profile, name='profile'),
    path('profile/edit', views.edit_profile, name='edit_profile'),
    path('profile/change_password', views.change_password, name='change_password'),
    path('test', views.test_page, name='test'),
    path('user_endpoint', views.user_endpoint, name='user_endpoint'),
    path('stock_data_endpoint/<stock_ticker>/', views.stock_data_endpoint,
         name='stock_data_endpoint'),
    path('insert_message/', views.insert_message,
         name='insert_message')
]
