from django.apps import AppConfig


class StockSiteAppConfig(AppConfig):
    name = 'stock_site'
