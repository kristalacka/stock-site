document
  .getElementById("search-bar")
  .addEventListener("keyup", function (event) {
    if (event.keyCode === 13) {
      window.location.href =
        "/dashboard/" +
        document.getElementById("search-bar").value.toUpperCase();
      return false;
    }
  });
