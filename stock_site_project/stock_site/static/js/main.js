function drawChart() {
  d3.selectAll("svg > *").remove();
  d3.json(stock_data_endpoint).then(function (data) {
    const transitionDuration = 300;

    var dateFormat = d3.timeParse("%Y-%m-%dT%H:%M:%S"); // time format: "2021-06-02T22:59:00"
    for (var i = 0; i < data.length; i++) {
      data[i].timestamp = dateFormat(data[i].timestamp);
    }
    const margin = { top: 15, right: 65, bottom: 100, left: 50 },
      boundingBox = document
        .getElementById("dashboardChart")
        .getBoundingClientRect();

    //dashboardChart
    var w = boundingBox.width - margin.left - margin.right;
    var h =
      window.innerHeight - boundingBox.top - margin.top - margin.bottom - 100;

    var svg = d3
      .select("#container")
      .attr("height", h + margin.top + margin.bottom)
      .attr("width", w + margin.left + margin.right)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    let dates = _.map(data, "timestamp");
    var xmin = d3.min(data.map((r) => r.timestamp.getTime()));
    var xmax = d3.max(data.map((r) => r.timestamp.getTime()));

    var xScale = d3.scaleLinear().domain([-1, dates.length]).range([0, w]);
    var xDateScale = d3.scaleQuantize().domain([0, dates.length]).range(dates);
    let xBand = d3
      .scaleBand()
      .domain(d3.range(-1, dates.length))
      .range([0, w])
      .padding(0.3);
    var xAxis = d3
      .axisBottom()
      .scale(xScale)
      .tickFormat(function (d) {
        d = dates[d];
        hours = d.getHours();
        minutes = (d.getMinutes() < 10 ? "0" : "") + d.getMinutes();
        amPM = hours < 13 ? "am" : "pm";
        return (
          hours + ":" + minutes + amPM
          // " " +
          // d.getDate() +
          // " " +
          // d.getMonth() +
          // " " +
          // d.getFullYear()
        );
      });

    svg
      .append("rect")
      .attr("id", "rect")
      .attr("width", w)
      .attr("height", h)
      .style("fill", "none")
      .style("pointer-events", "all")
      .attr("clip-path", "url(#clip)");

    var gX = svg
      .append("g")
      .attr("class", "axis x-axis")
      .attr("transform", "translate(0," + h + ")")
      .call(xAxis);

    gX.selectAll(".tick text").call(wrap, xBand.bandwidth());

    var ymin = d3.min(data.map((r) => r.candle_low));
    var ymax = d3.max(data.map((r) => r.candle_high));
    var yScale = d3.scaleLinear().domain([ymin, ymax]).range([h, 0]).nice();
    var yAxis = d3.axisLeft().scale(yScale);

    var gY = svg.append("g").attr("class", "axis y-axis").call(yAxis);

    var chartBody = svg
      .append("g")
      .attr("class", "chartBody")
      .attr("clip-path", "url(#clip)");

    // draw rectangles
    let candles = chartBody
      .selectAll(".candle")
      .data(data)
      .enter()
      .append("rect")
      .attr("x", (d, i) => xScale(i) - xBand.bandwidth())
      .attr("class", "candle")
      .attr("y", (d) => yScale(Math.max(d.price_open, d.price_close)))
      .attr("width", xBand.bandwidth())
      .attr("height", (d) =>
        d.price_open === d.price_close
          ? 1
          : yScale(Math.min(d.price_open, d.price_close)) -
            yScale(Math.max(d.price_open, d.price_close))
      )
      .attr("fill", (d) =>
        d.price_open === d.price_close
          ? "silver"
          : d.price_open > d.price_close
          ? "red"
          : "green"
      );

    // draw high and low
    let stems = chartBody
      .selectAll("g.line")
      .data(data)
      .enter()
      .append("line")
      .attr("class", "stem")
      .attr("x1", (d, i) => xScale(i) - xBand.bandwidth() / 2)
      .attr("x2", (d, i) => xScale(i) - xBand.bandwidth() / 2)
      .attr("y1", (d) => yScale(d.candle_high))
      .attr("y2", (d) => yScale(d.candle_low))
      .attr("stroke", (d) =>
        d.price_open === d.price_close
          ? "white"
          : d.price_open > d.price_close
          ? "red"
          : "green"
      );

    svg
      .append("defs")
      .append("clipPath")
      .attr("id", "clip")
      .append("rect")
      .attr("width", w)
      .attr("height", h);

    const extent = [
      [0, 0],
      [w, h],
    ];

    var resizeTimer;
    var zoom = d3
      .zoom()
      .scaleExtent([1, 100])
      .translateExtent(extent)
      .extent(extent)
      .on("zoom", zoomed)
      .on("zoom.end", zoomend);

    svg.call(zoom);

    function zoomed() {
      var t = d3.event.transform;
      let xScaleZ = t.rescaleX(xScale);

      let hideTicksWithoutLabel = function () {
        d3.selectAll(".xAxis .tick text").each(function (d) {
          if (this.innerHTML === "") {
            this.parentNode.style.display = "none";
          }
        });
      };

      gX.call(
        d3.axisBottom(xScaleZ).tickFormat((d, e, target) => {
          if (d >= 0 && d <= dates.length - 1) {
            d = dates[d];
            hours = d.getHours();
            minutes = (d.getMinutes() < 10 ? "0" : "") + d.getMinutes();
            amPM = hours < 13 ? "am" : "pm";
            return (
              hours + ":" + minutes + amPM
              // " " +
              // d.getDate() +
              // " " +
              // months[d.getMonth()] +
              // " " +
              // d.getFullYear()
            );
          }
        })
      );

      candles
        .attr("x", (d, i) => xScaleZ(i) - (xBand.bandwidth() * t.k) / 2)
        .attr("width", xBand.bandwidth() * t.k);
      stems.attr(
        "x1",
        (d, i) => xScaleZ(i) - xBand.bandwidth() / 2 + xBand.bandwidth() * 0.5
      );
      stems.attr(
        "x2",
        (d, i) => xScaleZ(i) - xBand.bandwidth() / 2 + xBand.bandwidth() * 0.5
      );

      hideTicksWithoutLabel();

      gX.selectAll(".tick text").call(wrap, xBand.bandwidth());
    }

    function zoomend() {
      var t = d3.event.transform;
      let xScaleZ = t.rescaleX(xScale);
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(function () {
        var xmin = new Date(xDateScale(Math.floor(xScaleZ.domain()[0])));
        xmax = new Date(xDateScale(Math.floor(xScaleZ.domain()[1])));
        filtered = _.filter(
          data,
          (d) => d.timestamp >= xmin && d.timestamp <= xmax
        );
        minP = +d3.min(filtered, (d) => d.candle_low);
        maxP = +d3.max(filtered, (d) => d.candle_high);
        buffer = (maxP - minP) * 0.05;
        yScale.domain([minP - buffer, maxP + buffer]);
        candles
          .transition()
          .duration(transitionDuration)
          .attr("y", (d) => yScale(Math.max(d.price_open, d.price_close)))
          .attr("height", (d) =>
            d.price_open === d.price_close
              ? 1
              : yScale(Math.min(d.price_open, d.price_close)) -
                yScale(Math.max(d.price_open, d.price_close))
          );

        stems
          .transition()
          .duration(transitionDuration)
          .attr("y1", (d) => yScale(d.candle_high))
          .attr("y2", (d) => yScale(d.candle_low));

        gY.transition()
          .duration(transitionDuration)
          .call(d3.axisLeft().scale(yScale));
      }, 500);
    }
  });
}

function wrap(text, width) {
  text.each(function () {
    var text = d3.select(this),
      words = text.text().split(/\s+/).reverse(),
      word,
      line = [],
      lineNumber = 0,
      lineHeight = 1.1,
      y = text.attr("y"),
      dy = parseFloat(text.attr("dy")),
      tspan = text
        .text(null)
        .append("tspan")
        .attr("x", 0)
        .attr("y", y)
        .attr("dy", dy + "em");
    while ((word = words.pop())) {
      line.push(word);
      tspan.text(line.join(" "));
      if (tspan.node().getComputedTextLength() > width) {
        line.pop();
        tspan.text(line.join(" "));
        line = [word];
        tspan = text
          .append("tspan")
          .attr("x", 0)
          .attr("y", y)
          .attr("dy", ++lineNumber * lineHeight + dy + "em")
          .text(word);
      }
    }
  });
}

drawChart();
window.addEventListener("resize", drawChart);
var updateTime = 60; // chart refresh time in seconds. todo: make dynamic
var intervalId = window.setInterval(function () {
  drawChart();
}, updateTime * 1000);
