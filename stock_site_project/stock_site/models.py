# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Chatter(models.Model):
    is_watching = models.BooleanField(blank=True, null=True)
    is_vip = models.BooleanField(blank=True, null=True)
    is_moderator = models.BooleanField(blank=True, null=True)
    is_blocked = models.BooleanField(blank=True, null=True)
    blocked_until = models.DateField(blank=True, null=True)
    fk_user_id = models.IntegerField()
    fk_stock_ticker_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'chatter'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey(
        'DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Line(models.Model):
    json = models.TextField(blank=True, null=True)
    type = models.CharField(max_length=8, blank=True, null=True)
    fk_user_sketch_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'line'


class Market(models.Model):
    name = models.TextField(blank=True, null=True)
    shortname = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'market'


class MarketResources(models.Model):
    title = models.TextField(blank=True, null=True)
    text = models.TextField(blank=True, null=True)
    category = models.TextField(blank=True, null=True)
    date_added = models.DateField(blank=True, null=True)
    last_edited = models.DateField(blank=True, null=True)
    is_public = models.BooleanField(blank=True, null=True)
    fk_user_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'market_resources'


class Message(models.Model):
    text = models.TextField(blank=True, null=True)
    timestamp = models.DateTimeField(blank=True, null=True)
    fk_user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    fk_stock_ticker_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'message'


class StockNews(models.Model):
    timestamp = models.DateTimeField(blank=True, null=True)
    headline = models.TextField(blank=True, null=True)
    link = models.TextField(blank=True, null=True)
    fk_stock_ticker_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'stock_news'


class StockTicker(models.Model):
    company_name = models.TextField(blank=True, null=True)
    ticker_name = models.TextField(blank=True, null=True)
    market = models.TextField(blank=True, null=True)
    last_close_price = models.FloatField(blank=True, null=True)
    views = models.IntegerField(blank=True, null=True)
    fk_market_id = models.IntegerField(blank=True, null=True)
    sector = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'stock_ticker'


class TickerPrice(models.Model):
    timestamp = models.DateTimeField(blank=True, null=True)
    candle_high = models.FloatField(blank=True, null=True)
    candle_low = models.FloatField(blank=True, null=True)
    price_open = models.FloatField(blank=True, null=True)
    price_close = models.FloatField(blank=True, null=True)
    fk_stock_ticker_id = models.IntegerField()
    volume = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ticker_price'
        unique_together = (('fk_stock_ticker_id', 'timestamp'),)


class User(models.Model):
    username = models.TextField(blank=True, null=True)
    email = models.TextField(blank=True, null=True)
    password_hash = models.TextField(blank=True, null=True)
    date_of_birth = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'user'


class UserFollowedTicker(models.Model):
    follow_date = models.DateField(blank=True, null=True)
    fk_stock_ticker_id = models.IntegerField()
    fk_user_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'user_followed_ticker'


class UserSketch(models.Model):
    date_edited = models.DateField(blank=True, null=True)
    date_started = models.DateField(blank=True, null=True)
    fk_stock_ticker_id = models.IntegerField()
    fk_user_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'user_sketch'
