--@(#) script.ddl

CREATE TABLE public.stock_ticker
(
	company_name TEXT,
	ticker_name TEXT,
	market TEXT,
	last_close_price double precision,
	views int,
	id SERIAL PRIMARY KEY
);

CREATE TABLE public.user
(
	username TEXT,
	email TEXT,
	password_hash TEXT,
	date_of_birth date,
	id SERIAL PRIMARY KEY
);

CREATE TABLE public.chatter
(
	is_watching boolean,
	is_vip boolean,
	is_moderator boolean,
	is_blocked boolean,
	blocked_until timestamp,
	id SERIAL PRIMARY KEY,
	fk_user_id integer NOT NULL,
	fk_stock_ticker_id integer NOT NULL
);

CREATE TABLE public.market_resources
(
	title TEXT,
	text TEXT,
	category TEXT,
	date_added timestamp,
	last_edited timestamp,
	is_public boolean,
	id SERIAL PRIMARY KEY,
	fk_user_id integer NOT NULL
);

CREATE TABLE public.message
(
	text TEXT,
	timestamp timestamp,
	id SERIAL PRIMARY KEY,
	fk_user_id integer NOT NULL,
	fk_stock_ticker_id integer NOT NULL
);

CREATE TABLE public.stock_news
(
	timestamp timestamp,
	headline TEXT,
	link TEXT,
	id SERIAL PRIMARY KEY,
	fk_stock_ticker_id integer NOT NULL
);

CREATE TABLE public.ticker_price
(
	timestamp timestamp,
	price double precision,
	candle_high double precision,
	candle_low double precision,
	price_open double precision,
	price_close double precision,
	id SERIAL PRIMARY KEY,
	fk_stock_ticker_id integer NOT NULL
);

CREATE TABLE public.user_followed_ticker
(
	follow_date timestamp,
	id SERIAL PRIMARY KEY,
	fk_stock_ticker_id integer NOT NULL,
	fk_user_id integer NOT NULL
);

CREATE TABLE public.user_sketch
(
	date_edited timestamp,
	date_started timestamp,
	id SERIAL PRIMARY KEY,
	fk_stock_ticker_id integer NOT NULL,
	fk_user_id integer NOT NULL
);

CREATE TABLE public.line
(
	json TEXT,
	type char (8),
	id SERIAL PRIMARY KEY,
	fk_user_sketch_id integer NOT NULL,
	CHECK(type in ('straight', 'spline', 'point', 'sequence', 'ruler'))
);
