--@(#) script.ddl

CREATE SCHEMA ER;

CREATE TABLE ER.Market
(
	id_Market integer,
	name TEXT,
	shortName TEXT,
	PRIMARY KEY(id_Market)
);


CREATE TABLE ER.StockTicker
(
	companyName TEXT,
	tickerName TEXT,
	market TEXT,
	sector TEXT,
	lastClosePrice double precision,
	views int,
	id_StockTicker integer,
	PRIMARY KEY(id_StockTicker)
);

CREATE TABLE ER.User
(
	username TEXT,
	email TEXT,
	passwordHash TEXT,
	dateOfBirth date,
	id_User integer,
	PRIMARY KEY(id_User)
);

CREATE TABLE ER.Chatter
(
	isWatching boolean,
	isVIP boolean,
	isModerator boolean,
	isBlocked boolean,
	blockedUntil date,
	id_Chatter integer,
	fk_Userid_User integer NOT NULL,
	fk_StockTickerid_StockTicker integer NOT NULL,
	PRIMARY KEY(id_Chatter),
	CONSTRAINT is FOREIGN KEY(fk_Userid_User) REFERENCES ER.User (id_User),
	CONSTRAINT belongsTo5 FOREIGN KEY(fk_StockTickerid_StockTicker) REFERENCES ER.StockTicker (id_StockTicker)
);

CREATE TABLE ER.MarketResources
(
	title TEXT,
	text TEXT,
	category TEXT,
	dateAdded date,
	lastEdited date,
	isPublic boolean,
	id_MarketResources integer,
	fk_Userid_User integer NOT NULL,
	PRIMARY KEY(id_MarketResources),
	CONSTRAINT creates FOREIGN KEY(fk_Userid_User) REFERENCES ER.User (id_User)
);

CREATE TABLE ER.Message
(
	text TEXT,
	timestamp date,
	id_Message integer,
	fk_Userid_User integer NOT NULL,
	fk_StockTickerid_StockTicker integer NOT NULL,
	PRIMARY KEY(id_Message),
	CONSTRAINT sends FOREIGN KEY(fk_Userid_User) REFERENCES ER.User (id_User),
	CONSTRAINT belongsTo FOREIGN KEY(fk_StockTickerid_StockTicker) REFERENCES ER.StockTicker (id_StockTicker)
);

CREATE TABLE ER.StockNews
(
	timestamp date,
	headline TEXT,
	link TEXT,
	id_StockNews integer,
	fk_StockTickerid_StockTicker integer NOT NULL,
	PRIMARY KEY(id_StockNews),
	CONSTRAINT has2 FOREIGN KEY(fk_StockTickerid_StockTicker) REFERENCES ER.StockTicker (id_StockTicker)
);

CREATE TABLE ER.TickerPrice
(
	timestamp date,
	price double precision,
	candleHigh double precision,
	candleLow double precision,
	priceOpen double precision,
	priceClose double precision,
	id_TickerPrice integer,
	fk_StockTickerid_StockTicker integer NOT NULL,
	PRIMARY KEY(id_TickerPrice),
	CONSTRAINT has FOREIGN KEY(fk_StockTickerid_StockTicker) REFERENCES ER.StockTicker (id_StockTicker)
);

CREATE TABLE ER.UserFollowedTicker
(
	followDate date,
	id_UserFollowedTicker integer,
	fk_StockTickerid_StockTicker integer NOT NULL,
	fk_Userid_User integer NOT NULL,
	PRIMARY KEY(id_UserFollowedTicker),
	CONSTRAINT belongsTo4 FOREIGN KEY(fk_StockTickerid_StockTicker) REFERENCES ER.StockTicker (id_StockTicker),
	CONSTRAINT belongsTo3 FOREIGN KEY(fk_Userid_User) REFERENCES ER.User (id_User)
);

CREATE TABLE ER.UserSketch
(
	dateEdited date,
	dateStarted date,
	id_UserSketch integer,
	fk_StockTickerid_StockTicker integer NOT NULL,
	fk_Userid_User integer NOT NULL,
	PRIMARY KEY(id_UserSketch),
	CONSTRAINT belongsTo2 FOREIGN KEY(fk_StockTickerid_StockTicker) REFERENCES ER.StockTicker (id_StockTicker),
	CONSTRAINT creates2 FOREIGN KEY(fk_Userid_User) REFERENCES ER.User (id_User)
);

CREATE TABLE ER.Line
(
	json TEXT,
	type char (8),
	id_Line integer,
	fk_UserSketchid_UserSketch integer NOT NULL,
	CHECK(type in ('straight', 'spline', 'point', 'sequence', 'ruler')),
	PRIMARY KEY(id_Line),
	CONSTRAINT comprises FOREIGN KEY(fk_UserSketchid_UserSketch) REFERENCES ER.UserSketch (id_UserSketch)
);
